﻿using UnityEngine;
using System.Collections.Generic;

public class ActionBar : MonoBehaviour {

	public HUD hud;
	public SkillSlots skillslot;
	public GUIStyle btnStyle;
	//public Texture2D btnTex;

	public Texture2D actionBar;
	public Rect barPosition;

	public Texture2D changeSkillsBox;
	public Rect changeSkillsBoxPosition;

	public Rect addSkillButtonPos;

	public Texture2D csbExitButtonTex;
	public Rect csbExitButton;

	public Vector2 csbScrollPosition = Vector2.zero;
	public Rect csbScrollView;
	public Rect csbInnerScrollView;
	public Rect csbTitleLabel;
	Rect outer;
	Rect inner;

	public bool openChangeSkillsMenu;

	public SkillSlots[] skillslotsArray; // actionBar SkillSLots array.

	public string[] stringList;	
	public int selectedEntry; 
	public int sel;
	public Rect skillIconPos;
	public Rect skillDescriptionPos;

	public string[] stringBar;
	public int selectedSlot;
	public int selSlot;

	int useThisSlot;

	public Rect stringBarPos;


	public float slotX;
	public float slotY;
	public float slotWidth;
	public float slotHeight;
	public float slotDistance;

	public Texture2D coolDownTex;



	void Awake()
	{

	}

	void Start ()
	{
		skillslotsArray = new SkillSlots[10];

		for(int sl = 0; sl < skillslotsArray.Length; sl++)
		{
			skillslotsArray[sl] = new SkillSlots();
		}

		stringList = new string[30];

		stringBar = new string[10];
		stringBar[0] = "1";
		stringBar[1] = "2";
		stringBar[2] = "3";
		stringBar[3] = "4";
		stringBar[4] = "5";
		stringBar[5] = "6";
		stringBar[6] = "7";
		stringBar[7] = "8";
		stringBar[8] = "9";
		stringBar[9] = "10";

		skillslotsArray[0].key = KeyCode.Alpha1;
		skillslotsArray[1].key = KeyCode.Alpha2;
		skillslotsArray[2].key = KeyCode.Alpha3;
		skillslotsArray[3].key = KeyCode.Alpha4;
		skillslotsArray[4].key = KeyCode.Alpha5;
		skillslotsArray[5].key = KeyCode.Alpha6;
		skillslotsArray[6].key = KeyCode.Alpha7;
		skillslotsArray[7].key = KeyCode.Alpha8;
		skillslotsArray[8].key = KeyCode.Alpha9;
		skillslotsArray[9].key = KeyCode.Alpha0;


	}

	void Update ()
	{
		//make the key for each slot correspond to the ifKeyDown(key) of each skill so when the player rpesses the 
		//key of the slot down, the corresponding skill fires.

		for (int k = 0; k <skillslotsArray.Length; k++)
		{
			if(skillslotsArray[k].skill != null)
			{
				skillslotsArray[k].skill.keyOnActionBar = skillslotsArray[k].key;
			}
		}

		//put hud.unlockskill list elements into the List
		for (int j = 0; j < hud.unlockedSkillList.Length; j++)
		{	
			if(hud.unlockedSkillList[j] != null)
			{
				stringList[j] = hud.unlockedSkillList[j].skillName;			
			}
		}
	
		selectedSlot = -1; //set the return selectedSlot in the actionBar to -1 so nothing is selected initially
		selectedEntry = -1;//set the return selectedEntry in the skills list to -1 so nothing is selected initially

		//set the size and spacing of the skillslots layout
		for(int i = 0; i < skillslotsArray.Length; i++)
		{
			skillslotsArray[i].slotPos.Set(slotX + i * (slotWidth + slotDistance), slotY , slotWidth, slotHeight); 
		// this will seperate the width of the skillslots by the number of skill slots "i" and space them apart by the distance)
		}

	}

	void OnGUI()
	{
		DrawActionBar();

		if (openChangeSkillsMenu == true)
		{
			ClickToMoveRobot.inGUI = true;
			DrawChangeSkillsBox();
		}

		//if there is a skill in a skillslot, then draw that skill's icon using its slot position
		for(int n = 0; n < skillslotsArray.Length; n++)
		{
			if(skillslotsArray[n].skill != null)
			GUI.DrawTexture(getRect (skillslotsArray[n].slotPos), skillslotsArray[n].skill.iconTexture);
		}

		//everytime a skill is on cooldown, display a black texture(with greyscale) and show the remaining cooldown time.
		for(int c = 0; c < skillslotsArray.Length; c++)
		{
			if(skillslotsArray[c].skill != null && skillslotsArray[c].skill.onCoolDown)
			{
				string cooldowntext = skillslotsArray[c].skill.coolDownCounter.ToString();
				GUI.DrawTexture (getRect (skillslotsArray[c].slotPos), coolDownTex);
				GUI.TextField (getRect (skillslotsArray[c].slotPos), cooldowntext, btnStyle);
			}
		}

		/*
		// This is how to make a button texture completely fill the button space
		if(GUI.Button (new Rect (100, 100, 100, 100), btnTex, btnStyle))
		{
			Debug.Log ("raawr");
		}

		if(GUI.Button (new Rect (Screen.width/2-100, Screen.height/2, 200, 50), btnTex, btnStyle))
		{
			Debug.Log ("raawrmeooow");
		}*/
	}

	void DrawActionBar()
	{
		//The actionBar Texture will resize itself with respect to the percentage of the screen size. 
		//If Resolution increases, the actionBar will still cover the same percentages of the screen.
		//position.x and position.y are a number between 0 - 1, and are W and H in the inspector.
		GUI.DrawTexture (new Rect (Screen.width * barPosition.x, Screen.height * barPosition.y, Screen.width * barPosition.width, Screen.height * barPosition.height), actionBar);

		/*for (int slots = 0; slots < skillslotsArray.Length; slots++)
		{
			stringBar[slots] = skillslotsArray[slots].skillName;
		}*/

		//make a grid of 10buttons to select repesenting each skillslot
		for(int slot = 0; slot < skillslotsArray.Length; slot++)
		{
			selectedSlot = GUI.SelectionGrid(getRect (stringBarPos), selectedSlot, stringBar, 10);
		}

		//Click any skillslot to change the skill contianed within it
		if(selectedSlot >= 0)
		{
			selSlot = selectedSlot;
			openChangeSkillsMenu = true;
		}

	}

	void DrawChangeSkillsBox()
	{
		//Draw the CSBox
		GUI.DrawTexture(new Rect(Screen.width * changeSkillsBoxPosition.x, Screen.height * changeSkillsBoxPosition.y, Screen.width * changeSkillsBoxPosition.width, Screen.height * changeSkillsBoxPosition.height), changeSkillsBox);

		//Draw the Add Skill button
		if(GUI.Button (getRect (addSkillButtonPos), "Add Skill"))
		{
			for(int q = 0; q < skillslotsArray.Length; q++)
			{
				if(skillslotsArray[q].skill == hud.unlockedSkillList[sel])
				{
					skillslotsArray[q].skill = null;
				}
			}

			skillslotsArray[selSlot].skill = hud.unlockedSkillList[sel];
		}

		//Draw the exit button for the CSBox
		if(GUI.Button (new Rect(Screen.width * csbExitButton.x, Screen.height * csbExitButton.y, Screen.width * csbExitButton.width, Screen.height * csbExitButton.height), csbExitButtonTex))
		{
			//when exit button pressed, switch off bollean that opens it
			openChangeSkillsMenu = false;
			ClickToMoveRobot.inGUI = false;
		}

		//draw the skillList from hud.unlockedSkillList[] using strings of names from stringList.
		DrawSkillList();

	}

	void DrawSkillList(){

		GUI.Label(new Rect(Screen.width * csbTitleLabel.x, Screen.height * csbTitleLabel.y, Screen.width * csbTitleLabel.width, Screen.height * csbTitleLabel.height), "Change skills ");

		Rect outer = getRect(csbScrollView); // the outside rectangle in which the scroll box resides

		//the inside rectagnle of the scroll (if making a list, place selectionGrid as the inside rectangle)
		Rect inner = new Rect (Screen.width * csbInnerScrollView.x, Screen.width * csbInnerScrollView.y, Screen.width * csbInnerScrollView.width, Screen.height * csbInnerScrollView.height);

		csbScrollPosition = GUI.BeginScrollView(outer, csbScrollPosition, inner);
		selectedEntry = GUI.SelectionGrid(inner, selectedEntry, stringList, 1);
		GUI.EndScrollView();

		if(selectedEntry >= 0)
		{
			sel = selectedEntry;
		}

		//draw the icon and description of the selected skill NT WORKING ATM
		if (sel >=0 && hud.unlockedSkillList[sel] != null)
		{
			GUI.DrawTexture(getRect (skillIconPos), hud.unlockedSkillList[sel].iconTexture);
			GUI.TextArea(getRect (skillDescriptionPos), hud.unlockedSkillList[sel].description);
		}

	}
	
	Rect getRect(Rect position)
	{
		return new Rect(Screen.width * position.x, Screen.height * position.y, Screen.width * position.width, Screen.height * position.height);
	}
























	}
