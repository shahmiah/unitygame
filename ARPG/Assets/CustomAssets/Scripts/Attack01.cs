﻿using UnityEngine;
using System.Collections;

public class Attack01 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			Destroy(gameObject, 1.5f);
		}

		if (other.tag == "Terrain")
		{
			Destroy (gameObject, 1.5f);
		}
	}

}
