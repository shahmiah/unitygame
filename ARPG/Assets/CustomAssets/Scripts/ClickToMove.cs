﻿using UnityEngine;
using System.Collections;

public class ClickToMove : MonoBehaviour {
	
	CharacterController controller;
	Vector3 destinationPosition = Vector3.zero;
	float destinationDistance = 0.0f;
	Vector3 gravPull = Vector3.zero;
	float gravity = 30.0f;
	float minMove = 0.5f;
	float maxMove = 500.0f;
	float speed = 8.0f;
	
	
	// Use this for initialization
	void Start () 
	{
		controller = GetComponent<CharacterController> ();
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		gravPull.y -= gravity * Time.deltaTime;
		controller.Move (gravPull * Time.deltaTime);
		MovementControl ();
		
	}
	
	private void MovementControl()
	{
		MoveHero();
		
		if (Input.GetMouseButton (0)) 
		{
			RotateHero();
		}
		
		destinationDistance = Vector3.Distance (destinationPosition, transform.position);
		
	}
	
	void MoveHero() 
	{
		if (destinationDistance >= minMove && destinationDistance <= maxMove) 
		{
			controller.SimpleMove(transform.forward * speed);
			Debug.DrawLine (destinationPosition, transform.position, Color.cyan);
		}
		
	}
	
	void RotateHero() 
	{
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane playerPlane = new Plane (Vector3.up, transform.position);
		float hitdist = 0.0f;
		
		if(playerPlane.Raycast(ray, out hitdist))
		{
			Vector3 targetPoint = ray.GetPoint(hitdist);
			destinationPosition = targetPoint;
			transform.rotation = Quaternion.LookRotation(targetPoint - transform.position);
		}
		
		
	}
	
	
	
}

