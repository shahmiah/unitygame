
using UnityEngine;
using System.Collections;

public class ClickToMoveRobot2 : MonoBehaviour {
	
	public Vector3 destinationPosition;
	public float speed;
	public CharacterController controller;
	//private float minMove = 0.5f;
	//private float maxMove = 500.0f;
	private float destinationDistance;
	public AnimationClip run;
	public AnimationClip idle;
	public Fighter player;
	public static bool attack;
	public static bool die;
	public static Vector3 cursorPosition;
	public static bool inGUI;


	// Use this for initialization
	void Start () 
	{
		destinationPosition = transform.position;

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!attack && !die && !player.usingSpecial && !inGUI)
		{
			Movement ();
		} 

		else
		{
			destinationPosition = transform.position;
		}

		destinationDistance = Vector3.Distance (destinationPosition, transform.position);

		//Debug.Log (destinationDistance);

		findCursorPosition();

	}

	void findCursorPosition()
	{
		RaycastHit cursorInfo;
		Ray rayForCursor = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Physics.Raycast (rayForCursor, out cursorInfo, 3000)) 
		{
			cursorPosition = cursorInfo.point;
		}

		//Debug.Log (cursorPosition);
	}



	void Movement() 
	{
		
		if(Input.GetMouseButton (0)) 
		{
			RotateCharacter();		

		}

		MoveCharacter(destinationDistance, speed);
			
	}
	
	void RotateCharacter() 
	{	
		//This will turn the character towards the position clicked by the player.
		RaycastHit hitinfo;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
		if (Physics.Raycast (ray, out hitinfo, 3000)) 
		{
			Debug.DrawLine (destinationPosition, transform.position, Color.red);

			if(hitinfo.collider.tag!= "Player" )
			{
			destinationPosition = hitinfo.point;
			}
			//|| hitinfo.collider.tag!= "Enemy"
			
			Quaternion newRotation = Quaternion.LookRotation (destinationPosition - transform.position);
			newRotation.x = 0f;
			newRotation.z = 0f;
			//Debug.Log(hitinfo.collider.tag);
			transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * 100);
			
			//Debug.Log ("I've rotated to: " + destinationPosition);
			
		}
	}
	
	public void MoveCharacter(float dist, float spd)
	{
		Debug.Log (dist);
		//This will move the character forward AFTER the charcter has turned to face the position clicked by player.
		if (dist > 3.0f) 
		{
			controller.SimpleMove (transform.forward * spd);
			GetComponent<Animation>().CrossFade(run.name);

		} 
		else 
		{
			GetComponent<Animation>().CrossFade(idle.name);

		}
	}
}