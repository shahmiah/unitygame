using UnityEngine;
using System.Collections;

public class ClickToMoveSM2 : MonoBehaviour {
	
	private Vector3 destinationPosition;
	public float speed;
	public CharacterController controller;
	//private float minMove = 0.5f;
	//private float maxMove = 500.0f;
	private float destinationDistance = 0.0f;

	
	// Use this for initialization
	void Start () 
	{
		destinationPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{

		Movement ();
		
	}
	
	void Movement() 
	{

		if(Input.GetMouseButton (0)) 
		{
			RotateCharacter();
		}
		
		MoveCharacter();
		
		destinationDistance = Vector3.Distance (transform.position, destinationPosition);
	}

	void RotateCharacter() 
	{	
		//This will turn the character towards the position clicked by the player.
		RaycastHit hitinfo;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
		if (Physics.Raycast (ray, out hitinfo, 3000)) 
		{
			Debug.DrawLine (destinationPosition, transform.position, Color.red);
			destinationPosition = hitinfo.point;
			
			
			
			Quaternion newRotation = Quaternion.LookRotation (destinationPosition - transform.position);
			newRotation.x = 0f;
			newRotation.z = 0f;
			
			transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * 100);
			
			//Debug.Log ("I've rotated to: " + destinationPosition);
			
		}
	}
	
	void MoveCharacter()
	{
		//This will move the character forward AFTER the charcter has turned to face the position clicked by player.
		
		if (destinationDistance >= 4.75f) 
		{

			controller.SimpleMove(transform.forward * speed);
			//Debug.Log (destinationDistance);
		} 

	}
}
