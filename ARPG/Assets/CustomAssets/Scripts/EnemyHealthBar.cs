﻿using UnityEngine;
using System.Collections;

public class EnemyHealthBar : MonoBehaviour {

	public Texture2D frame;
	public Rect framePosition;


	public Texture2D healthBar;
	public Rect healthBarPosition;
	public float healthBarWidth;
	public float healthBarHeight;
	public float horizontalDistance;
	public float verticalDistance;

	public Fighter player;
	public Mob target;
	public float healthPercentage;

	void Start () 
	{	
	}
	

	void Update () 
	{	
		if(player.opponent != null) //if there is a target, target get mob script of the player's opponent and apply if statement
		{
		
		target = player.opponent.GetComponent<Mob> (); // target is accessing the Mob script of the opponent in the fighter script
		healthPercentage = (float)target.health / target.maxHealth;
		}
		else
		{
			healthPercentage = 0;
			target = null; // disable if statement if there is no enemy
		}
	}

	void OnGUI() 
	{
		if (target != null && player.inContactRange)
		{
		
		drawFrame ();
		drawHealthBar ();

		}
	}

	void drawFrame()
	{
		framePosition.x = (Screen.width - framePosition.width) / 2;

		framePosition.width = Screen.width * 0.3125f;
		framePosition.height = Screen.height * 0.0195f;
		GUI.DrawTexture (framePosition, frame);
	}

	void drawHealthBar()
	{
		healthBarPosition.x = framePosition.x + framePosition.width * horizontalDistance;
		healthBarPosition.y = framePosition.y + framePosition.height * verticalDistance;
		healthBarPosition.width = framePosition.width * healthBarWidth * healthPercentage;
		healthBarPosition.height = framePosition.height * healthBarHeight;
		GUI.DrawTexture (healthBarPosition, healthBar);
	}

	
}

