﻿using UnityEngine;
using System.Collections;

public class Fighter : MonoBehaviour {



	public GameObject opponent;
	public AnimationClip normalAttackAnim;
	public AnimationClip death;

	public int maxHealth;
	public int health;


	public int maxMana;
	public int mana;

	public int damage;
	public double impactTime; // time at which the player will reduce the enemies health.
	public bool impacted;
	//public KeyCode NormalAtkKey;
	public CharacterController controller;

	public float rangeToEnemy;
	public float weaponsRange;
	public float contactRange;
	public bool inWeaponsRange;
	public bool inContactRange;
	public float moveSpeed = 10;
	public bool mustCloseDistance;
	public int projectiles;

	bool started;//for death animation
	bool ended;//for death animation

	//public float combatEscapeTime;
	//public float countDown;

	public bool usingSpecial;

	// Use this for initialization
	void Start () 
	{
		health = maxHealth;
		mana = maxMana;
	}
	
	// Update is called once per frame
	void Update () 
	{


		if(!usingSpecial)
		{
			attackFunction();
		}
		
		if(Input.GetMouseButtonDown(1) && inContactRange && opponent != null && !inWeaponsRange)
		{	
			//ClickToMoveRobot.attack = true;
			//moveSpeed = transform.GetComponent<ClickToMoveRobot>().speed;
			//controller.SimpleMove(Vector3.forward * moveSpeed);
			mustCloseDistance = true;//boolean to call automatic movement towards the enemy if not in range

		}

		closeDistance();// player runs towards enemy when mustCloseDistance is true, mustCloseDistance is set to false in Mob.cs and Terrain.cs.


		if(rangeToEnemy <= weaponsRange && opponent != null)
		{
			transform.GetComponent<ClickToMoveRobot>().destinationPosition = transform.position;
			mustCloseDistance = false;
		}

		inRangeCheck();
		dieMethod ();

	}

	void closeDistance()
	{
		if(mustCloseDistance == true)
		{
			transform.LookAt(opponent.transform);	
			transform.GetComponent<ClickToMoveRobot>().MoveCharacter(rangeToEnemy, transform.GetComponent<ClickToMoveRobot>().speed/6);
			//speed component is multipled when MoveCharacter is called in Update(), DO NOT KNOW WHY, MUST INVSETIGATE FURTHER ONE DAY.
		}
	}


	public void attackFunction()
	{

		//if(Input.GetKey(NormalAtkKey) && inContactRange)
		if(Input.GetMouseButton(1) && inContactRange && opponent != null && inWeaponsRange)
		{
	
			GetComponent<Animation>().Play(normalAttackAnim.name);
			ClickToMoveRobot.attack = true;
			transform.LookAt(opponent.transform); //make the fighter/player face the enemy when the mouse is over it.

		}
		impact();

		if (GetComponent<Animation>()[normalAttackAnim.name].time > 0.6 * GetComponent<Animation>()[normalAttackAnim.name].length) //returns impaced(damage dealt) to false so the fighter can deal damage again
		{
			ClickToMoveRobot.attack = false;
			impacted = false;

		}
	}


	public void resetAttackFunction()

	{
		ClickToMoveRobot.attack = false;
		impacted = false;
		GetComponent<Animation>().Stop (normalAttackAnim.name);
	}

	void impact()
	{
		if(opponent != null && GetComponent<Animation>().IsPlaying(normalAttackAnim.name) && !impacted)
		{
			if(GetComponent<Animation>()[normalAttackAnim.name].time > GetComponent<Animation>()[normalAttackAnim.name].length * impactTime && GetComponent<Animation>()[normalAttackAnim.name].time < 0.6 * GetComponent<Animation>()[normalAttackAnim.name].length)
			{			
				//countDown = combatEscapeTime + 2;
				//CancelInvoke ("combatEscapeCountDown");
				//combatEscapeRepeat();
				opponent.GetComponent<Mob>().getHit(damage); //wizardry. opponent variable gets the Mob script and passes the damage through the getHit method to themob.
				impacted = true;
				Instantiate(Resources.Load ("Attack01"), new Vector3(opponent.transform.position.x, opponent.transform.position.y +2.5f, opponent.transform.position.z), Quaternion.identity);
				Instantiate(Resources.Load ("BulletSinglePrefab"), new Vector3(transform.position.x, transform.position.y +2.5f, transform.position.z), transform.rotation);
			}
		}
	}
	/*public void combatEscapeRepeat()
	{
		if (inRange())
		{
		countDown = combatEscapeTime + 2;
		InvokeRepeating("combatEscapeCountDown", 0, 1);
		}
	}

	public void combatEscapeCountDown() 
	{
		countDown = countDown - 1;
		if (countDown == 0) 
		{
			CancelInvoke ("combatEscapeCountDown");
		}
		
	}*/

	public void inRangeCheck()

	{
		if (opponent != null) 
		{
			rangeToEnemy = Vector3.Distance (opponent.transform.position, transform.position);

			if (rangeToEnemy <= weaponsRange) 
			{
				inWeaponsRange = true;
			}
			else 
			{
				inWeaponsRange = false;
			}

			if (rangeToEnemy <= contactRange)
			{
				inContactRange = true;
			}
			else 
			{
				inContactRange = false;
			}
		}
	}
	/*
	bool inAttackRange()
	{
		if (Vector3.Distance (opponent.transform.position, transform.position) <= attackRange) 
		{
			return true;
		}
		else 
		{
			return false;
		}

		//return (Vector3.Distance (opponent.transform.position, transform.position) <= range); This is the shorter way of writing the boolean.
	}*/

	public void getHit(int damage) 
	{
		health = health - damage;
		if (health < 0) 
		{
			health = 0;
		}
	}

	public void loseMana(int manaCost)
	{
		mana = mana - manaCost;
		if (mana < 0) 
		{
			mana = 0;
		}
	}


	public bool isDead()
	{
		return (health == 0);

	}

	public void dieMethod() 
	{
		if (isDead () && !ended) 

			if (!started) 
			{
				GetComponent<Animation>().Play(death.name);
				started = true;
				ClickToMoveRobot.die = true;
			}

			if(started && !GetComponent<Animation>().IsPlaying(death.name))
			{
			Debug.Log ("You have died! GAHAHAHHAH!");
			ended = true;
			started = false;
			ClickToMoveRobot.die = false;
			}
	}



}

