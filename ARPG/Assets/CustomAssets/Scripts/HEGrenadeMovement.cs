﻿using UnityEngine;
using System.Collections;

public class HEGrenadeMovement : MonoBehaviour 
{

	private float throwSpeed; //speed at which the grenade willb e thrown
	
	private float throwHorizontalDistance; //how far the grenade will travel
	private float throwVerticalDistance; // how high the grenade will reach
	
	private float throwSpeedVerticalComponent; // verticalSpeed component used to calculate how high the grenade will reach
	//private float throwSpeedHorizontalComponent; //horzontalSpeed component wil be determined because we know the speed and time already

	//private float travelTime;
	private float throwAngle = 30;
	private float angle;
	private float gravity = 10;

	public double grenadeDamage;
	public float blastRadius;

	//public Collider[] hitColliders;

	//private float velocity;
	//private float angle = throwAngle * Mathf.Deg2Rad;


	void Start () 
	{


	}
	
	// Update is called once per frame
	void Update () 
	{
		angle = throwAngle * Mathf.Deg2Rad; //CHANGE ANGLE TO DEGREES
		
		//throwHorizontalDistance = Vector3.Distance (SpecialAttacks.grenadePosition, SpecialAttacks.playerPosition); //HORIZONTAL RANGE/DISTANCE
		throwHorizontalDistance = Vector3.Distance (Skill.grenadePosition, Skill.playerPosition); //HORIZONTAL RANGE/DISTANCE

		throwSpeed = Mathf.Sqrt ((throwHorizontalDistance * gravity) / Mathf.Sin (2 * angle)); //calculate ACTUAL INITIAL SPEED

		throwVerticalDistance = (throwSpeedVerticalComponent / (2 * 10)) +2.0f;

		throwSpeedVerticalComponent = throwSpeed * Mathf.Sin(angle); //VERTICAL SPEED
		//throwSpeedHorizontalComponent = throwSpeed * Mathf.Cos(angle); // HORIZONTAL SPEED
		
		//travelTime = throwHorizontalDistance / throwSpeedHorizontalComponent;

		//apply vertical and horozontal components per meters per second;		
		transform.Translate (0 ,throwVerticalDistance * Time.deltaTime, throwHorizontalDistance * Time.deltaTime);


	}


	public void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Enemy")
		{
			//other.GetComponent<Mob>().getHit (grenadeDamage);
			Destroy(gameObject);
			Instantiate(Resources.Load ("Attack01"), new Vector3(GetComponent<Collider>().transform.position.x, GetComponent<Collider>().transform.position.y +2.5f, GetComponent<Collider>().transform.position.z), Quaternion.identity);
		}

		if(other.tag == "Terrain")
		{
			Destroy(gameObject);
			Instantiate(Resources.Load ("Attack01"), new Vector3(transform.position.x, transform.position.y +2.5f, transform.position.z), Quaternion.identity);
		}
		
		 //This method will check if there are other colliders in area of the grenade.

		AOECheck();
	}

	public void AOECheck()
	{
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, blastRadius);

		for(int i = 0; i < hitColliders.Length; i++)
		{
			Debug.Log (hitColliders[i]);
			if (hitColliders[i].tag == "Enemy")
			{
				hitColliders[i].GetComponent<Mob>().getHit (grenadeDamage);
			}
			//hitColliders[i].GetComponent<Mob>().getHit(100);
		}

	}

}

/*	for(int i = 0; i < hitColliders.Length; i++)
		{
			hitColliders[i].GetComponent<Mob>().getHit(100);
		}
		
		foreach(Collider collider in hitColliders)
		{
			hitColliders[i].GetComponent<Mob>().getHit(100);
		}
		
		while(i < hitColliders.Length)
		{
			hitColliders[i].SendMessage("projectileDamage");
			i++;
		}*/

/*
	public void projectileDamage()
	{
		GetComponent<Mob>().getHit (Skill.projectileDamage);
	}*/

