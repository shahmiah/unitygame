﻿using UnityEngine;
using System.Collections.Generic;

public class HUD : MonoBehaviour {

	//GUI variables
	public ActionBar actionBar;

	public Texture2D hpFrame;
	public Rect hpFramePosition;

	public Texture2D hpFiller;
	public Rect hpFillerPosition;

	public Texture2D mpFrame;
	public Rect mpFramePosition;

	public Texture2D mpFiller;
	public Rect mpFillerPosition;

	public Texture2D xpBar;
	public Rect xpBarPosition;

	public Texture2D xpBarFiller;
	public Rect xpBarFillerPosition;

	public Texture2D SkillTreeButtonTexture;
	public Rect SkillTreeButtonPos;

	public Texture2D OptionsMenuButtonTexture;
	public Rect OptionsMenuButtonPos;

	public Texture2D CharacterStatsMenuButtonTexture;
	public Rect CharacterStatsMenuButtonPos;

	public Texture2D InventoryButtonTexture;
	public Rect InventoryButtonPos;

	public Texture2D ChatMenuButtonTexture;
	public Rect ChatMenuButtonPos;

	public Texture2D MapButtonTexture;
	public Rect MapButtonPos;

	private float playerHealthPercentage;
	private float playerManaPercentage;

	// external variables
	public Fighter player;

	//SkillTree variables
	public Texture2D skillTreeTexture;
	public Rect skillTreePos;
	public bool openSkillTree;
	public int availableSkillPoints;

	public Texture2D stExitButtonTex;
	public Rect stExitButtonPos; 

	public Rect stSkCounter1Pos;
	public int skill1points = 0;
	public Rect stSkCounter2Pos;
	public int skill2points = 0;

	public Texture2D stSkCounter1AddTex;
	public Rect stSkCounter1Add;
	public Texture2D stSkCounter1MinusTex;
	public Rect stSkCounter1Minus;

	public Texture2D stSkCounter2AddTex;
	public Rect stSkCounter2Add;
	public Texture2D stSkCounter2MinusTex;
	public Rect stSkCounter2Minus;

	//public List<SkillTemplate> availableSkills;
	public Skill[] skillTreeList;
	public Skill[] unlockedSkillList;

	public PopUpList[] popupList;
	public float listX;
	public float listY;
	public float listWidth;
	public float listHeight;
	public float listDistance;

	public bool openInventory;

	void Awake()
	{
		skillTreeList = new Skill[30]; // skillTreeList is the base skillArray which houses all the skills for the
		// game.
		skillTreeList = GameObject.FindGameObjectWithTag("Player").GetComponents<Skill>(); //get all the skills from 
		//"Player"	
		unlockedSkillList = new Skill[30]; // array to hold all "unlocked" skills from the Skill Tree.
		
		//popupList = new PopUpList[11];

	}
	
	void Start () 
	{		
	}

	void Update () 
	{	
		playerHealthPercentage = (float)player.health / player.maxHealth;
		playerManaPercentage = (float)player.mana / player.maxMana;

	}

	void OnGUI ()
	{
		DrawhpFrame();
		DrawhpFiller();
		DrawmpFrame();
		DrawmpFiller();
		DrawxpBar();
		DrawxpBarFiller();

		SkillTreeButton();
		OptionsMenuButton();
		CharacterStatsMenuButton();
		InventoryMenuButton();
		ChatMenuButton();
		MapButton();

		if(openSkillTree == true)
		{
			ClickToMoveRobot.inGUI = true;
			DrawSkillTree();
		}
	}

	void DrawhpFrame() 
	{
		GUI.DrawTexture (new Rect (Screen.width * hpFramePosition.x, Screen.height * hpFramePosition.y, Screen.width * hpFramePosition.width, Screen.height * hpFramePosition.height), hpFrame);
	}

	void DrawhpFiller()
	{
		GUI.DrawTexture (new Rect (Screen.width * hpFillerPosition.x, Screen.height * hpFillerPosition.y, Screen.width * hpFillerPosition.width, Screen.height * (hpFillerPosition.height * -playerHealthPercentage)), hpFiller);
	}

	void DrawmpFrame() 
	{		 
		GUI.DrawTexture (new Rect (Screen.width * mpFramePosition.x, Screen.height * mpFramePosition.y, Screen.width * mpFramePosition.width, Screen.height * mpFramePosition.height), mpFrame);
	}

	void DrawmpFiller()
	{
		GUI.DrawTexture (new Rect (Screen.width * mpFillerPosition.x, Screen.height * mpFillerPosition.y, Screen.width * mpFillerPosition.width, Screen.height * (mpFillerPosition.height * -playerManaPercentage)), mpFiller);
	}
	
	void DrawxpBar()
	{
		GUI.DrawTexture (new Rect (Screen.width * xpBarPosition.x, Screen.height * xpBarPosition.y, Screen.width * xpBarPosition.width, Screen.height * xpBarPosition.height), xpBar);
	}

	void DrawxpBarFiller()
	{
		GUI.DrawTexture (new Rect(Screen.width * xpBarFillerPosition.x, Screen.height * xpBarFillerPosition.y, Screen.width * xpBarFillerPosition.width, Screen.height * xpBarFillerPosition.height), xpBarFiller); 
	}

	void SkillTreeButton()
	{
		if (GUI.Button (new Rect(Screen.width * SkillTreeButtonPos.x, Screen.height * SkillTreeButtonPos.y, Screen.width * SkillTreeButtonPos.width, Screen.height * SkillTreeButtonPos.height), SkillTreeButtonTexture))
		{
			openSkillTree = true;
		}
	}

	void OptionsMenuButton()
	{
		if (GUI.Button (new Rect(Screen.width * OptionsMenuButtonPos.x, Screen.height * OptionsMenuButtonPos.y, Screen.width * OptionsMenuButtonPos.width, Screen.height * OptionsMenuButtonPos.height), OptionsMenuButtonTexture))
		{

		}
	}

	void CharacterStatsMenuButton()
	{
		if (GUI.Button (new Rect(Screen.width * CharacterStatsMenuButtonPos.x, Screen.height * CharacterStatsMenuButtonPos.y, Screen.width * CharacterStatsMenuButtonPos.width, Screen.height * CharacterStatsMenuButtonPos.height), CharacterStatsMenuButtonTexture))
		{

		}
	}

	void InventoryMenuButton()
	{
		if (GUI.Button (new Rect(Screen.width * InventoryButtonPos.x, Screen.height * InventoryButtonPos.y, Screen.width * InventoryButtonPos.width, Screen.height * InventoryButtonPos.height), InventoryButtonTexture))
		{
			openInventory = true;
		}
	}

	void ChatMenuButton()
	{
		if (GUI.Button (new Rect(Screen.width * ChatMenuButtonPos.x, Screen.height * ChatMenuButtonPos.y, Screen.width * ChatMenuButtonPos.width, Screen.height * ChatMenuButtonPos.height), ChatMenuButtonTexture))
		{

		}
	}

	void MapButton()
	{
		if (GUI.Button (new Rect(Screen.width * MapButtonPos.x, Screen.height * MapButtonPos.y, Screen.width * MapButtonPos.width, Screen.height * MapButtonPos.height), MapButtonTexture))
		{

		}
	}

	void DrawSkillTree()
	{
		GUI.DrawTexture(new Rect(Screen.width * skillTreePos.x, Screen.height * skillTreePos.y, Screen.width * skillTreePos.width, Screen.height * skillTreePos.height), skillTreeTexture);
		
		if(GUI.Button(new Rect(Screen.width * stExitButtonPos.x, Screen.height * stExitButtonPos.y, Screen.width * stExitButtonPos.width, Screen.height * stExitButtonPos.height), stExitButtonTex))
		{
			openSkillTree = false;
			ClickToMoveRobot.inGUI = false;
		}

		GUI.TextArea(new Rect(Screen.width * stSkCounter1Pos.x, Screen.height * stSkCounter1Pos.y, Screen.width * stSkCounter1Pos.width, Screen.height * stSkCounter1Pos.height), (skill1points.ToString()));

		if(GUI.Button (new Rect(Screen.width * stSkCounter1Add.x, Screen.height * stSkCounter1Add.y, Screen.width * stSkCounter1Add.width, Screen.height * stSkCounter1Add.height), stSkCounter1AddTex))
		{
			skill1points = skill1points + 1;
			if(skill1points == 1)
			{
				unlockedSkillList[0] = skillTreeList[0];
			}
		}

		if(GUI.Button (new Rect(Screen.width * stSkCounter1Minus.x, Screen.height * stSkCounter1Minus.y, Screen.width * stSkCounter1Minus.width, Screen.height * stSkCounter1Minus.height), stSkCounter1MinusTex))
		{
			skill1points = skill1points - 1;
			if(skill1points <= 0)
			{
				skill1points = 0;
				unlockedSkillList[0] = null;
			}
		}

		GUI.TextArea(new Rect(Screen.width * stSkCounter2Pos.x, Screen.height * stSkCounter2Pos.y, Screen.width * stSkCounter2Pos.width, Screen.height * stSkCounter2Pos.height), (skill2points.ToString()));

		if(GUI.Button (new Rect(Screen.width * stSkCounter2Add.x, Screen.height * stSkCounter2Add.y, Screen.width * stSkCounter2Add.width, Screen.height * stSkCounter2Add.height), stSkCounter1AddTex))
		{
			skill2points = skill2points + 1;
			if(skill2points == 1)
			{
				unlockedSkillList[1] = skillTreeList[1];
			}
		}
		
		if(GUI.Button (new Rect(Screen.width * stSkCounter2Minus.x, Screen.height * stSkCounter2Minus.y, Screen.width * stSkCounter2Minus.width, Screen.height * stSkCounter2Minus.height), stSkCounter1MinusTex))
		{
			skill2points = skill2points - 1;
			if(skill2points <= 0)
			{
				skill2points = 0;
				unlockedSkillList[1] = null;
			}
		}


	}


	Rect getRect(Rect position)
	{
		return new Rect(Screen.width * position.x, Screen.height * position.y, Screen.width * position.width, Screen.height * position.height);

	}



















}
