﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Inventory : MonoBehaviour {

	public HUD hud;
	public MainItemsList mainItemsList;

	public List<Item> inventoryList;

	public Rect inventoryPos;
	public Texture2D inventoryTex;

	public Rect invExitButton;
	public Texture2D inExtButtonTex;

	public Texture2D slotTex;

	public int slotRows = 5;
	public int slotColumns = 10;

	public InventorySlots[,] inventorySlots;

	public float slotX;
	public float slotY;
	public float slotWidth;
	public float slotHeight;
	public float slotDistanceX;
	public float slotDistanceY;

	public bool movingItem;

	public Item itemToMove;
	public Item secondItemToMove;
	public Rect originalItemPosition;
	public int rowOfItem;
	public int columnOfItem;

	/*public List<Armor> Headgear;
	public List<Armor> ChestArmor;
	public List<Armor> Gloves;
	public List<Armor> Legs;
	public List<Armor> Belt;*/

	public Armor[] Headgear = new Armor[1];
	public Armor[] HeadAccessory1 = new Armor[1];
	public Armor[] HeadAccessory2 = new Armor[1];

	public Armor[] Chest = new Armor[1];
	public Armor[] ChestAccessory1 = new Armor[1];
	public Armor[] ChestAccessory2 = new Armor[1];
	public Armor[] ChestAccessory3 = new Armor[1];
	public Armor[] ChestAccessory4 = new Armor[1];

	public Armor[] Glove = new Armor[1];
	public Armor[] GloveAccessory1 = new Armor[1];
	public Armor[] GloveAccessory2 = new Armor[1];

	public Armor[] Boots = new Armor[1];
	public Armor[] BootAccessory1 = new Armor[1];
	public Armor[] BootAccessory2 = new Armor[1];

	public Armor[] Belt = new Armor[1];
	public Armor[] BeltAccessory1 = new Armor[1];
	public Armor[] BeltAccessory2 = new Armor[1];


	void Awake()
	{

		inventorySlots = new InventorySlots[slotRows,slotColumns];
	}


	void Start () 
	{
		InitializeSlots();

		for (int i = 0; i< mainItemsList.armorList.Count; i++)
		{
			inventoryList.Add (mainItemsList.armorList[i]);		
		}
	}

	void Update () 
	{	
		SetTilePositions();//make the inventory tiles
		AssignItemPositions(); // make each item in the inventoryList have the same position as a tile
		 
	}

	void OnGUI()
	{
		// if the inventory button is clicked open the inventory texture and make the inventory slots grid.
		if(hud.openInventory == true)
		{
			ClickToMoveRobot.inGUI = true;
			OpenInventory();
			DrawItems();//draw items
			DrawSlots();// draw slots and allows item movement

		}
	}

	void DrawItems()
	{
		//draw each item texture in its correct position
		for(int c = 0; c < inventoryList.Count; c++)
		{
			GUI.DrawTexture(getRect(inventoryList[c].itemPosition), inventoryList[c].itemTexture);
		}

	}

	void DrawSlots()
	{

		//make the inventorySlots a grid using InventorySlot[].position
		for(int r = 0; r < slotRows; r++)
		{
			for(int c = 0; c < slotColumns; c++)
			{
				GUI.DrawTexture(getRect(inventorySlots[r, c].position), slotTex);
			}
		}
		
		for(int r = 0; r < slotRows; r++)
		{
			for(int c = 0; c < slotColumns; c++)
			{				

				//////TO SELECT AN ITEM FOR MOVEMENT FROM ONE SLOT TO ANOTHER USING LEFT CLICK/////
				//DO NOT USE Contains(Input.mousePosition), ALWAYS USE Contains(Event.current.mousePosition)//
				if (inventorySlots[r, c].occupied == true && getRect(inventorySlots[r, c].position).Contains(Event.current.mousePosition) && Event.current.isMouse && Input.GetMouseButtonDown(0))
				{
					if(movingItem == false)
					{	
						for(int it = 0 ; it < inventoryList.Count; it++)//go through the inventoryList 
						{// and find the item that has the same position as the slot that was clicked
							if(inventoryList[it].itemPosition == inventorySlots[r, c].position)
							{
								itemToMove = inventoryList[it];
								inventoryList.RemoveAt(it);
								movingItem = true;// set boolean to move items to true;
								inventorySlots[r, c].occupied = false;
								originalItemPosition = inventorySlots[r, c].position;//copy position of item so it can be used when the move is cancelled using right click
								rowOfItem = r; // copy r of item clicked incase of right click cancellation
								columnOfItem = c;//copy c of item click incase of right click cancellation
								return;
							}							
						}
					}
				}

				/////TO CANCEL THE MOVEMENT OF THE ITEM USING RIGHT CLICK/////
				if (Event.current.isMouse && Input.GetMouseButtonDown(1) && movingItem == true)
				{	
					movingItem = false;
					inventoryList.Add (itemToMove);
					inventoryList[inventoryList.Count-1].itemPosition = originalItemPosition;
					itemToMove = null;
					inventorySlots[rowOfItem, columnOfItem].occupied = true;
					return;
				}
			}
		}
		//////TO PUT AN ITEM INTO AN EMPTY SLOT IN THE INVENTORY////////////
		for(int r = 0; r < slotRows; r++)
		{
			for(int c = 0; c < slotColumns; c++)
			{
				if (getRect(inventorySlots[r, c].position).Contains(Event.current.mousePosition) && Event.current.isMouse && Input.GetMouseButtonDown(0))
				{
					if(inventorySlots[r, c].occupied == false && !inventoryList.Contains(itemToMove) && itemToMove != null && movingItem == true)
					{	
						movingItem = false;
						inventoryList.Add (itemToMove);
						inventoryList[inventoryList.Count-1].itemPosition = inventorySlots[r, c].position;
						itemToMove = null;
						inventorySlots[r, c].occupied = true;
						return;
					}
				}
			}
		}

		//////TO MOVE AN ITEM INTO ANOTHER OCCUPIED SLOT AND REPLACE THE ITEM CONTAINED///////////////

		for(int r = 0; r < slotRows; r++)
		{
			for(int c = 0; c < slotColumns; c++)
			{
				if (getRect(inventorySlots[r, c].position).Contains(Event.current.mousePosition) && Event.current.isMouse && Input.GetMouseButtonDown(0))
				{
					if(inventorySlots[r, c].occupied == true && !inventoryList.Contains(itemToMove) && itemToMove != null && movingItem == true)
					{	
						for(int it = 0 ; it < inventoryList.Count; it++)//go through the inventoryList 
						{// and find the item that has the same position as the slot that was clicked
							if(inventoryList[it].itemPosition == inventorySlots[r, c].position)
							{
								secondItemToMove = inventoryList[it];
								inventoryList.RemoveAt(it);
								// set boolean to move items to true;
							}
						}

						inventoryList.Add (itemToMove);
						itemToMove = secondItemToMove;
						secondItemToMove = null;
						inventoryList[inventoryList.Count-1].itemPosition = inventorySlots[r, c].position;						
						inventorySlots[r, c].occupied = true;
						return;
					}
				}
			}
		}
			
		if(movingItem == true && itemToMove != null) //DRAW THE ITEM BEING MOVED///
		{
			//if we are moving an item, the boolean is set true, and the icon texture of the item is dragged around by the mouse pointer.
			itemToMove.itemPosition.Set (Event.current.mousePosition.x - 5.0f, Event.current.mousePosition.y - 5.0f, slotWidth * Screen.width, slotHeight * Screen.height);
			GUI.DrawTexture(itemToMove.itemPosition, itemToMove.itemTexture);
		}

	}

	void SetTilePositions()
	{
		//set the position of each inventorySlot tile
		for(int r = 0; r < slotRows; r++)
		{
			for (int c = 0; c < slotColumns; c++)
			{
				inventorySlots[r,c].position.Set (slotX + r * (slotWidth + slotDistanceX), slotY + c * (slotHeight + slotDistanceY), slotWidth, slotHeight);
			}			
			
		}

	}

	void InitializeSlots()
	{
		/// make a new inventorySlot instance for each tile.
		for(int r = 0; r < slotRows; r++)
		{
			for (int c = 0; c < slotColumns; c++)
			{
				inventorySlots[r,c] = new InventorySlots();
			}			
			
		}
	}

	void AssignItemPositions()
	{
		// for every Item in the inventory list, run through the inventory grid and assign each grid position to one 
		//item, checking if both the item is assigned and the slot is not occupied. After a grid position is assigned
		//to an item break out of the loop.
		foreach (Item item in inventoryList)
		{
			if(item.assignedSlot == false)
			{
				for(int r = 0; r < slotRows; r++)
				{
					for(int c = 0; c < slotColumns; c++)
					{
						if(inventorySlots[r, c].occupied == false)
						{
							item.itemPosition = inventorySlots[r, c].position;
							inventorySlots[r, c].occupied = true;
							item.assignedSlot = true;
							return;
						}
					}
				}
			}
		}
	}

	void OpenInventory()
	{
		GUI.DrawTexture(getRect (inventoryPos), inventoryTex);

		if(GUI.Button (getRect(invExitButton), inExtButtonTex))
		{
			hud.openInventory = false;
			ClickToMoveRobot.inGUI = false;
		}
	}

	Rect getRect(Rect position)
	{
		return new Rect (Screen.width * position.x, Screen.height * position.y, Screen.width * position.width, Screen.height * position.height);
	}
}
