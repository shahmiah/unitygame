﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item 
{	
	public string itemName;
	public int damage;
	public int durability;
	public int range;
	public int defense;
	public int itemLevel;
	public int type;
	public int quantity;
	public Texture2D itemTexture;

	public Rect itemPosition;
	public bool assignedSlot;

	/*public int itemX;
	public int itemY;
	public int itemWidth;
	public int itemHeight;*/

	void Start () 
	{
	
	}
	

	void Update () 
	{
	
	}
}
