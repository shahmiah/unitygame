﻿using UnityEngine;
using System.Collections;

public class LevelingUp : MonoBehaviour {

	public int level;
	public int exp;
	public Fighter player;


	void Start () 
	{
	
	}
	

	void Update ()
	{

		LevelUp ();
	}

	void LevelUp() 
	{
		if (exp >= Mathf.Pow(level, 2) + 100) 
		{
			level = level + 1;
			exp = exp - (int)(Mathf.Pow(level, 2) + 100);
			Debug.Log ("Congratulations! You have leveled to " + level + "!");
			levelEffect ();
		}
	}

	void levelEffect() 
	{
		player.maxHealth = player.maxHealth + 50;
		player.damage = player.damage + 5;
		player.health = player.maxHealth;

	}

}
