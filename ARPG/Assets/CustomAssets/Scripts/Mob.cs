﻿using UnityEngine;
using System.Collections;

public class Mob : MonoBehaviour {

	public float speed;
	public float range;
	public Transform player;
	public CharacterController controller;
	public AnimationClip idle;
	public AnimationClip run;
	public AnimationClip attackClip;
	public AnimationClip death;
	public LevelingUp playerLevel;

	private Fighter opponent;

	private int stunTime;
	public int dotDamage;
	public int dotDuration;
	public bool isUnderDoT;

	private float rangeToPlayer;
	public float agroRange;
	public float mobWeaponsRange;
	public bool inAgroRange;
	public bool inMobWeaponsRange;

	public int health;
	public int maxHealth;
	public int damage;
	public double impactTime = 0.36;
	public bool impacted;



	// Use this for initialization
	void Start () 
	{
		health = maxHealth;
		opponent = player.GetComponent<Fighter> (); //opponent will access fighter script  
	}

	// Update is called once per frame
	void Update () 
	{
		inRangeCheck ();

		if (!isDead()) 
		{	
			if (stunTime <=0)
			{
				if(inAgroRange) 
				{
					if (!inMobWeaponsRange) 
					{
					chase ();
					} 
					else 
					{
						GetComponent<Animation>().Play (attackClip.name);
						attack ();
					}
				}
			}
			else 
			{
				
			}
		}

		else 
		{
			dieMethod ();
		}

		if (GetComponent<Animation>() [attackClip.name].time > GetComponent<Animation>() [attackClip.name].length * 0.9) 
		{
			impacted = false;
		}	
			
	}

	public void getStunned(int seconds)
	{
		stunTime = seconds;
		InvokeRepeating("stunCountDown", 0, 1);
	}

	void stunCountDown()
	{
		stunTime = stunTime - 1;

		if(stunTime <= 0) 
		{
			CancelInvoke("stunCountDown");
		}
	}

	public void getDoT(int damage, int seconds)
	{		 
		dotDamage = damage;
		dotDuration = seconds;
		InvokeRepeating("dotCountDown", 0.0001f, 1.0f);
	}

	public void dotCountDown()
	{
		dotDuration = dotDuration - 1;
		isUnderDoT = true;

		if (isUnderDoT == true) 
		{
			getHit(dotDamage);
		}
		else
		{

		}

		if (dotDuration <= 0) 
		{
			CancelInvoke("dotCountDown");
			isUnderDoT = false;
		}
		
	}

	void attack()
	{
		if (GetComponent<Animation>()[attackClip.name].time > GetComponent<Animation>()[attackClip.name].length * impactTime && !impacted && GetComponent<Animation>()[attackClip.name].time < GetComponent<Animation>()[attackClip.name].length * 0.9)
		{
			opponent.getHit(damage);
			impacted = true;
		}
			
	}

	void inRangeCheck() 
	{
		rangeToPlayer =	Vector3.Distance (transform.position, player.position);

		if(rangeToPlayer <= agroRange)
		{
			inAgroRange = true;
		}
		else
		{
			inAgroRange = false;
		}

		if(rangeToPlayer <=	mobWeaponsRange)
		{
			inMobWeaponsRange = true;						
		}
		else
		{
			inMobWeaponsRange = false;
		}
	}

	void chase() 
	{
		GetComponent<Animation>().CrossFade (run.name);
		transform.LookAt (player.position);
		controller.SimpleMove (transform.forward * speed);

	}

	void dieMethod()
	{
		GetComponent<Animation>().Play(death.name);

		if (GetComponent<Animation>() [death.name].time > GetComponent<Animation>() [death.name].length * 0.9)
		 
		{
			playerLevel.exp = playerLevel.exp + 100;
			Destroy(gameObject);
		}
	}

	bool isDead()
	{
		return(health <= 0); 
				
	}

	void OnMouseOver()
	{
		player.GetComponent<Fighter> ().opponent = gameObject;
	}


	void OnMouseUpAsButton()
	{
		if(GetComponent<Collider>().tag == "Enemy")
		{
			player.GetComponent<Fighter> ().opponent = gameObject;
		}

	}

	public void getHit(double damage) //mob takes this damage from the player.
	{
		health = health - (int)damage;
		if (health < 0) 
		{
			health = 0;
		}

	}






}
