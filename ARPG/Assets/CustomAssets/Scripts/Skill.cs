﻿using UnityEngine;
using System.Collections;

public class Skill : MonoBehaviour
{
	public string skillName;
	public int damage;
	public int dotDamage;
	public int effectDuration;

	public int coolDown;
	public int coolDownCounter;
	public int manaCost;
	public int stunDuration;

	public bool opponentBased;

	public KeyCode keyOnActionBar;
	
	public AnimationClip animationClip;
	public Texture2D iconTexture;
	
	public bool onCoolDown;
	public string description;
	//public int coolDownCounter;
	
	public Mob enemy;
	public Fighter player;
	public ActionBar actionBar;

	public static Vector3 playerPosition;
	public static Vector3 grenadePosition;

	public string nameOfPrefab;
	//public Vector3 prefabPosition;


	void Start () 
	{

	}
	
	void Update () 
	{

		if(Input.GetKeyDown(keyOnActionBar))
		{

			if(opponentBased)
			{
				enemy = player.opponent.GetComponent<Mob>();

				if(!onCoolDown)
				{
					player.usingSpecial = true;
					player.resetAttackFunction();
					player.transform.LookAt(enemy.transform);
					GetComponent<Animation>().Play(animationClip.name);

					manaCostMethod(manaCost);
					damageMethod(damage);

					effectDurationMethod(dotDamage, effectDuration);
					stunDurationMethod(stunDuration);
					coolDownMethod();
				}
				else
				{
					Debug.Log (skillName + " is on cool down");
				}
			}

			else 
			{

				if(!onCoolDown)
				{

					player.usingSpecial = true;
					player.resetAttackFunction();
					Quaternion cursorRotation = Quaternion.LookRotation(ClickToMoveRobot.cursorPosition - transform.position);
					cursorRotation.x = 0;
					cursorRotation.z = 0;
					transform.LookAt (ClickToMoveRobot.cursorPosition);
				
					manaCostMethod(manaCost);
					coolDownMethod();

					playerPosition = player.transform.position;
					grenadePosition = ClickToMoveRobot.cursorPosition;
				
					GetComponent<Animation>().Play (animationClip.name);	
					Instantiate(Resources.Load (nameOfPrefab), new Vector3 (transform.position.x , transform.position.y + 3.0f, transform.position.z + 2.0f), cursorRotation);
				
				}
				else 
				{
					Debug.Log (skillName + " is on cool down");
				}
			}


			//opponentBasedMethod(); transform.position.x , transform.position.y + 3.0f, transform.position.z

		}

		if(GetComponent<Animation>()[animationClip.name].time > 0.3 * GetComponent<Animation>()[animationClip.name].length)
		{
			player.usingSpecial = false;
		}

	}
			
	public void damageMethod(int damageDealt)
	{
		//method for passing damage
		enemy.getHit (damageDealt);
	}
	
	public void coolDownMethod()
	{
		onCoolDown = true;
		coolDownCounter = coolDown + 2;
		InvokeRepeating ("CoolDownCount", 0, 1);
	}
	
	public void CoolDownCount()
	{
		coolDownCounter = coolDownCounter - 1;
		if(coolDownCounter == 0)
		{
			onCoolDown = false;
			CancelInvoke ("CoolDownCount");
		}
	}

	public void manaCostMethod(int manaCost)
	{
		//method for counting coolDown

		player.loseMana(manaCost);
	}
	
	public void stunDurationMethod(int stunDuration)
	{
		//method for passing stunDuration
		//this.stunDuration = stunDuration;
		enemy.getStunned(stunDuration);
	}
	
	public void effectDurationMethod(int dotDamage, int effectDuration)
	{
		//method for counting effectDuration
		//this.effectDuration = effectDuration;
		//enemy.dotDamage = damage;
		//enemy.dotDuration = effectDuration;
		enemy.getDoT (dotDamage, effectDuration);
	}
	
	/*public void opponentBasedMethod()
	{
		//method for checking if opponentBased
		if(opponentBased && enemy == null)
		{
			Debug.Log ("Please select a target");

		}
		else
		{
			enemy = player.opponent.GetComponent<Mob>();
		}

		if(!opponentBased)
		{

		}
	}*/

}

