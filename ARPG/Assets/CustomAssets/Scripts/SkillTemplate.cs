﻿using UnityEngine;
using System.Collections;

public class SkillTemplate
{
	public string skillName;
	public int damage;
	public int coolDown;
	public int manaCost;
	public int stunDuration;
	public int effectDuration;
	public KeyCode keyOnActionBar;

	public AnimationClip animationClip;
	public Texture2D iconTexture;

	public bool onCoolDown;
	//public int coolDownCounter;

	public Mob enemy;
	public Fighter player;
	public ActionBar actionBar;

	public SkillTemplate()
	{

	}

	// general constructor for skills
	public SkillTemplate (string skillName, int damage, int coolDown, int manaCost, int stunDuration, int effectDuration)
	{
		this.skillName = skillName;
		this.damage = damage;
		this.coolDown = coolDown;
		this.manaCost = manaCost;
		this.stunDuration = stunDuration;
		this.effectDuration = effectDuration;

	}

	public void damageMethod(int damageDealt)
	{
		//method for passing damage

		enemy.getHit (damageDealt);

		//override method to enemy.dotDamage(damage) if DoT
	}

	public void manaCostMethod(int manaCost)
	{
		//method for counting coolDown
		player.loseMana(manaCost);
	}

	public void stunDurationMethod(int stunDuration)
	{
		//method for passing stunDuration
		enemy.getStunned(stunDuration);
	}

	public void effectDurationMethod()
	{
		//method for counting effectDuration
		enemy.dotDuration = effectDuration;
	}



}

/*public virtual void CoolDownInvoke()
	{
		coolDownCounter = coolDown + 2;
		//InvokeRepeating ("CoolDownCount", 0, 1);
	}
	
	public virtual void CoolDownCount()
	{
		coolDownCounter = coolDownCounter - 1;
		if(coolDownCounter == 0)
		{
			onCoolDown = false;
			//CancelInvoke ("CoolDownCount");
		}*/
































