﻿using UnityEngine;
using System.Collections;

public class SpecialAttacks : MonoBehaviour 
{
	public Texture2D stunPicture;
	public Texture2D psnPicture;
	public Texture2D grndPicture;

	public KeyCode stunKey;
	public KeyCode psnKey;
	public KeyCode grndKey;

	public Mob enemy;
	public Fighter player;

	public AnimationClip stunAttackAnim;
	public bool stunOnCoolDown;
	private int stunCoolDownTime;
	public int stunCoolDownDuration;

	public AnimationClip poisonAttackAnim;
	public AnimationClip grndAttackAnim;

	public static Vector3 playerPosition;
	public static Vector3 grenadePosition;
	//public static Vector3 playerPosition;


	void Start () 
	{

	}
	

	void Update () 
	{

		if (Input.GetKeyDown(stunKey))
		{

			enemy = player.opponent.GetComponent<Mob>();
				
			if(!stunOnCoolDown)
			{
				if(player.inWeaponsRange)
				{
					player.usingSpecial = true;
					player.resetAttackFunction();
					transform.LookAt(enemy.transform);

					GetComponent<Animation>().Play(stunAttackAnim.name);
					enemy.getStunned (8);
					stunOnCoolDown = true;
					stunCoolDownInvoke();
				}

				else if(!player.inWeaponsRange)
				{
					Debug.Log ("You are not in range");
				}
			}

			if(stunOnCoolDown)
			{
				Debug.Log ("Stun is on cooldown");
			}
		}

		if (Input.GetKeyDown (psnKey)) 
		{
			enemy = player.opponent.GetComponent<Mob>();
				
			if(player.inWeaponsRange)
			{
				player.usingSpecial = true;
				player.resetAttackFunction();
				transform.LookAt (enemy.transform);

				GetComponent<Animation>().Play (poisonAttackAnim.name);
				enemy.getDoT(10, 5);
			}

			else if(!player.inWeaponsRange)
			{
				Debug.Log ("You are not in range");
			}
		}

		if (Input.GetKeyDown (grndKey))
		{
			player.usingSpecial = true;
			player.resetAttackFunction();
			transform.LookAt (ClickToMoveRobot.cursorPosition);
				
			player.loseMana(30);
			Quaternion cursorRotation = Quaternion.LookRotation(ClickToMoveRobot.cursorPosition - transform.position);
			cursorRotation.x = 0;
			cursorRotation.z = 0;

			playerPosition = player.transform.position;
			grenadePosition = ClickToMoveRobot.cursorPosition;
				
			GetComponent<Animation>().Play (grndAttackAnim.name);	
			Instantiate(Resources.Load ("GrenadePrefab"), new Vector3 (transform.position.x , transform.position.y + 3.0f, transform.position.z), cursorRotation);
				
		}

		//Debug.Log (grenadePosition);

		if (GetComponent<Animation>()[stunAttackAnim.name].time > 0.8 * GetComponent<Animation>()[stunAttackAnim.name].length) 
		{
			player.usingSpecial = false;
		}

		if (GetComponent<Animation>()[poisonAttackAnim.name].time > 0.8 * GetComponent<Animation>()[poisonAttackAnim.name].length)
		{
			player.usingSpecial = false;
		}

		if (GetComponent<Animation>()[grndAttackAnim.name].time > 0.8 * GetComponent<Animation>()[poisonAttackAnim.name].length)
		{
			player.usingSpecial = false;
		}

	}//update ends here

	void stunCoolDownInvoke()
	{
		stunCoolDownTime = stunCoolDownDuration + 2;
		InvokeRepeating ("stunCoolDownCount", 0, 1);
	}

	void stunCoolDownCount()
	{
		stunCoolDownTime = stunCoolDownTime - 1;
		if(stunCoolDownTime == 0)
		{
			stunOnCoolDown = false;
			CancelInvoke ("stunCoolDownCount");
		}
	}

	//void stunAttack() 
	//{
		//if(Input.GetKey(KeyCode.Alpha1))
		//{
			//player.usingSpecial = true;
			//enemy = player.opponent.GetComponent<Mob>();
			//enemy.getStunned (20);

		//}
	//}
}


